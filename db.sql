DROP DATABASE IF EXISTS `db`;
CREATE DATABASE `db`;

CREATE TABLE `db`.`locations` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT
);

CREATE TABLE `db`.`categories` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` TEXT
);

INSERT INTO `db`.`locations` (`name`, `description`)
VALUES ('Chief', 'Director office'), ('IT', 'IT department'), ('Marketing', 'Marketing department'), ('Finance', 'Finance department');

INSERT INTO `db`.`categories` (`name`, `description`)
VALUES ('Computers', 'Computers'), ('Laptops', 'Laptops'), ('Office', 'Office equipment'), ('Domestic', 'Domestic appliances');

CREATE TABLE `db`.`assets` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `category_id` INT NOT NULL,
    `location_id` INT NOT NULL,
    `description` TEXT,
    `registration_date` DATE,
    INDEX `FK_category_idx` (`category_id`),
    INDEX `FK_location_idx` (`location_id`),
    CONSTRAINT `FK_category`
		FOREIGN KEY (`category_id`)
        REFERENCES `db`.`categories` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
    CONSTRAINT `FK_location`
		FOREIGN KEY (`location_id`)
        REFERENCES `db`.`locations` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

INSERT INTO `db`.`assets` (`name`, `category_id`, `location_id`, `description`, `registration_date`)
VALUES ('Computer', 1, 1, 'CPU i7, RAM 8Gb', '2017-04-15'),
	('Computer', 1, 2, 'CPU i7, RAM 16Gb', '2017-03-18'),
	('Computer', 1, 2, 'CPU i7, RAM 16Gb', '2017-03-18'),
	('Computer', 1, 3, 'CPU i5, RAM 8Gb', '2017-04-10'),
	('Computer', 1, 3, 'CPU i5, RAM 8Gb', '2017-04-10'),
	('Computer', 1, 4, 'CPU i3, RAM 4Gb', '2017-02-15'),
	('Computer', 1, 4, 'CPU i3, RAM 4Gb', '2017-02-15'),
	('Notebook', 2, 1, 'CPU i7, RAM 8Gb', '2017-02-23'),
	('Notebook', 2, 2, 'CPU i7, RAM 16Gb', '2017-01-15'),
	('Printer', 3, 1, 'Canon LBP-2900', '2017-04-15'),
	('Printer', 3, 2, 'Canon LBP-2900', '2017-03-18'),
	('MFU', 3, 2, 'Canon MF-237 dw', '2017-03-18'),
	('MFU', 3, 3, 'Canon MF-237 dw', '2017-04-10'),
	('MFU', 3, 4, 'Canon MF-237 dw', '2017-02-15'),
	('Air conditioning', 4, 1, 'Chigo', '2017-02-14'),
	('Air conditioning', 4, 2, 'AUX', '2017-03-17'),
	('Banknote counter', 4, 4, 'Magner', '2017-02-10'),
	('Lamp', 4, 4, 'Philips', '2017-02-10'),
	('Water cooler', 4, 2, 'JeWin', '2017-04-18'),
	('Water cooler', 4, 4, 'JeWin', '2017-04-18');